﻿using UnityEngine;

public class Father : Human {
    // Father is the thing which son inherits from, but father itself is considered human, as such it inherits from Human
    // A parent has their own genetic code which they pass down to their child, this is called inheritance

    void Start () {
        WhatAmI ();
    }

    public Father () {
        // A father has their own name, a surname and a unique genetic code.
        firstName = "Father";
        surname = "Childs";

        genetics = 1234;
    }

    public override void WhatAmI () {
        // Overrides the WhatAmI function, for a father specific debug line
        Debug.Log ($"I am {name} {surname}, my genetic code is {genetics}, I am the father of Son");
    }
}