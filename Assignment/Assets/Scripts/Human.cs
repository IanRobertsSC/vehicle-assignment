﻿using UnityEngine;

public class Human : MonoBehaviour {
    // A human has a name{String}, a surname{String} and a genetic code{int}
    protected string firstName = "Human";
    protected string surname = "Being";
    protected int genetics = 0;

    public virtual void WhatAmI () {
        // Debugs a line to state that this is in fact the human
        Debug.Log ("I am human");
    }
}