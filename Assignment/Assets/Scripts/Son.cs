﻿using UnityEngine;

public class Son : Father {
    public Son () {
        // A son has their own name, but inherits their father's surname and genetic code
        firstName = "Son";
    }

    public override void WhatAmI () {
        // Overrides the WhatAmI function, for a son specific debug line
        Debug.Log ($"I am {firstName} {surname}, my genetic code is {genetics}, I am the son of Father");
    }
}