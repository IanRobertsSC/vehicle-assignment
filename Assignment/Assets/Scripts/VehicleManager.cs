﻿using UnityEngine;

public class VehicleManager : MonoBehaviour {
    [Header ("Wheels")]
    // Front Wheels
    [SerializeField] GameObject frontLeft;
    [SerializeField] GameObject frontRight;

    // Front Wheel colliders
    WheelCollider frontLeftCol;
    WheelCollider frontRightCol;

    // Back Wheels
    [SerializeField] GameObject backLeft;
    [SerializeField] GameObject backRight;

    // Back Wheel colliders
    WheelCollider backLeftCol;
    WheelCollider backRightCol;

    // Car Values
    [Header ("Vehicle Controls")]
    // All car values are set to public so they can be influenced throught the singleton
    public float accelerationFactor = 50;
    public float maxTurnAngle = 10;
    public float stoppingSpeed = 100;
    //Stopping power refers to how strong the brakes can be compared ti the maximum speed of the car
    [SerializeField] float stoppingPower = 2;

    [SerializeField] float maxAcceleration = 1000;
    float currentAcceleration = 0;

    float stopTorque = 0;

    // This bool stops the vehicle from moving if brakes are on
    bool stopped = false;

    // The singleton
    public static VehicleManager instance;

    // Camera
    [Header ("Camera Controls")]
    [SerializeField] Transform cameraMover;
    [SerializeField] float cameraSpeed = 10;

    void Awake () {
        // Sets the current instance of this script as the singleton value
        instance = this;

        // Gets the wheel colliders which are found on empty objects parented to the wheels
        frontLeftCol = frontLeft.transform.GetChild (0).GetComponent<WheelCollider> ();
        frontRightCol = frontRight.transform.GetChild (0).GetComponent<WheelCollider> ();
        backLeftCol = backLeft.transform.GetChild (0).GetComponent<WheelCollider> ();
        backRightCol = backRight.transform.GetChild (0).GetComponent<WheelCollider> ();
    }

    void Update () {
        //If the CameraMover has a reference then apply the following
        if (cameraMover != null) {
            //Moves the camera to where the car is on the X and Z Axis, while keeping its own Y Axis
            //Means bumps wont come in the way of the camera
            cameraMover.position = Vector3.Lerp (cameraMover.position, transform.GetChild (0).position, cameraSpeed * Time.deltaTime);
            cameraMover.rotation = Quaternion.Euler (transform.eulerAngles.x, transform.GetChild (0).eulerAngles.y, transform.eulerAngles.z);
        }

        if (Input.GetKey (KeyCode.Space)) {
            // If space is pressed down the vehicle would proceed to slow down, until it finally stops
            Brake ();
        } else {
            // If space isn't being held down the stopped bool is set as false, thus allowing the vehicle to receive input again
            stopped = false;

            // If space isn't being held down brakeTorque is reset to 0, thus allowing vehicle to build torque again
            stopTorque = 0;
            frontLeftCol.brakeTorque = 0;
            frontRightCol.brakeTorque = 0;
            backLeftCol.brakeTorque = 0;
            backRightCol.brakeTorque = 0;
        }

        if (!stopped) {
            // Front wheel driving
            // W is forward, and S is backward
            if (Input.GetAxis ("Vertical") > 0 || Input.GetAxis ("Vertical") < 0) {
                // Only the front wheels are motorized
                if (currentAcceleration < maxAcceleration) {
                    currentAcceleration += accelerationFactor;
                }
                frontLeftCol.motorTorque = currentAcceleration * Input.GetAxis ("Vertical");
                frontRightCol.motorTorque = currentAcceleration * Input.GetAxis ("Vertical");
            } else {
                // Applies brakes while not accelerating, since space isnt being held down, this means torque can still be built up
                Brake ();
            }
        }

        // Even while stopped the wheels can be rotated
        // A is left, and D is right
        if (Input.GetAxis ("Horizontal") > 0 || Input.GetAxis ("Horizontal") < 0) {
            frontLeftCol.steerAngle = maxTurnAngle * Input.GetAxis ("Horizontal");
            frontRightCol.steerAngle = maxTurnAngle * Input.GetAxis ("Horizontal");
        }

        //Rotates the Wheel Mesh, to the same rotation as the Wheel Collider
        frontLeft.transform.localEulerAngles = new Vector3 (frontLeft.transform.localEulerAngles.x, frontLeftCol.steerAngle - frontLeft.transform.localEulerAngles.z + 90, frontLeft.transform.localEulerAngles.z);
        frontRight.transform.localEulerAngles = new Vector3 (frontRight.transform.localEulerAngles.x, frontRightCol.steerAngle - frontRight.transform.localEulerAngles.z + 90, frontRight.transform.localEulerAngles.z);

        frontLeft.transform.Rotate (0, frontLeftCol.rpm / 60 * 360 * Time.deltaTime, 0);
        frontRight.transform.Rotate (0, frontRightCol.rpm / 60 * 360 * Time.deltaTime, 0);
        backLeft.transform.Rotate (0, backLeftCol.rpm / 60 * 360 * Time.deltaTime, 0);
        backRight.transform.Rotate (0, backRightCol.rpm / 60 * 360 * Time.deltaTime, 0);

    }

    void Brake () {
        // The stopped bool is used to ensure that torque can't be built while the vehicle is stopped
        stopped = true;

        // Sets motorTorque to 0, thus stopping the vehicle from moving directly after braking
        backRightCol.motorTorque = 0;
        backLeftCol.motorTorque = 0;
        frontRightCol.motorTorque = 0;
        frontLeftCol.motorTorque = 0;
        currentAcceleration = 0;

        // Increases brakeTorque over time, thus slowly coming to a stop
        if (stopTorque <= maxAcceleration * stoppingPower) {
            stopTorque += stoppingSpeed;
        }

        frontLeftCol.brakeTorque = stopTorque;
        frontRightCol.brakeTorque = stopTorque;
        backLeftCol.brakeTorque = stopTorque;
        backRightCol.brakeTorque = stopTorque;
    }
}