# Vehicle Assignment

This unity project contains a vehicle, which moves using Unity's standard Horizontal (To rotate its wheels) and Vertical (To accelerate or reverse) axes, the vehicle uses its brakes whenever the Space key is pressed, as well as whenever it isn't accelerating.

# Inheritance and Polymorphism

Additionally this project also contains three additional scripts Human, Father and Son; to show my understanding of inheritance and polymorphism. The human is the base script, where the father inherits its functions and values from, and the son in turn inherits from the father's values.

# Final Words

Thanks for considering me for this position.